package pl.sda.Gender;

import java.util.Scanner;

public class Gender {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String linia = scanner.nextLine();

        try{
            GenderValue wpisana = GenderValue.valueOf(linia.trim());
            System.out.println("Wpisano: " + wpisana);
        }catch(IllegalArgumentException e){
            System.out.println("There is no such option.");
        }


    }
}
